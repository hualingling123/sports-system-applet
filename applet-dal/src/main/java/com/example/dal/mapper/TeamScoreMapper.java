package com.example.dal.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.dal.domain.Team;
import com.example.dal.domain.TeamScore;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TeamScoreMapper extends BaseMapper<TeamScore> {
    List<TeamScore> teamRank(@Param("competitionId") int id);
}
