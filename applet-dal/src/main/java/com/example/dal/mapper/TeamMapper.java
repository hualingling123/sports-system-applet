package com.example.dal.mapper;

import com.example.dal.domain.Team;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hualingling
 * @since 2023-06-28
 */
@Mapper
public interface TeamMapper extends BaseMapper<Team> {
    int joinTeam(@Param("userId") String userId,@Param("groupId")int groupId);
    int changeTeamNum(@Param("groupId") int groupId);
    int quitTeam(@Param("userId") String userId);
    int changeTeamNum1(@Param("groupId") int groupId);
}
