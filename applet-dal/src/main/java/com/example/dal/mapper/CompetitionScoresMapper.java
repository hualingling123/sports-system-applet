package com.example.dal.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.dal.domain.CompetitionScores;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hualingling
 * @since 2023-07-07
 */
@Mapper
public interface CompetitionScoresMapper extends BaseMapper<CompetitionScores> {

}
