package com.example.dal.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.dal.domain.ChoiceQuestion;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@Mapper
public interface ChoiceQuestionMapper extends BaseMapper<ChoiceQuestion> {

}
