package com.example.dal.mapper;

import com.example.dal.domain.AuditRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@Mapper
public interface AuditRecordMapper extends BaseMapper<AuditRecord> {

}
