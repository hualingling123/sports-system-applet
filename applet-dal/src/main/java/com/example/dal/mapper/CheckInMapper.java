package com.example.dal.mapper;

import com.example.dal.domain.CheckIn;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.dal.domain.UserInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@Mapper
public interface CheckInMapper extends BaseMapper<CheckIn> {
    List<UserInfo> getList(@Param("userid") String userid, @Param("competitionId") String competitionId);
}
