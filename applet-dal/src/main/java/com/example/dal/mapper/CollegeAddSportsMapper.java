package com.example.dal.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.dal.domain.AthleteApplication;
import com.example.dal.domain.AuditRecord;
import com.example.dal.domain.UserInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CollegeAddSportsMapper extends BaseMapper<UserInfo> {
}
