package com.example.dal.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.dal.domain.CompetitionApplication;
import com.example.dal.domain.CompetitionInformation;
import com.example.dal.domain.UserInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hualingling
 * @since 2023-07-03
 */
@Mapper
public interface CompetitionApplicationMapper extends BaseMapper<CompetitionApplication> {

}
