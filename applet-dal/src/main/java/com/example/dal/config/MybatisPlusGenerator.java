package com.example.dal.config;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Collections;

/**
 * @Author: hualingling
 * @Date:2023/06/06/15:53
 */

public class MybatisPlusGenerator {
    public static void main(String[] args) {
        String url = "jdbc:mysql://101.43.5.104:33060/sports-admin?useUnicode=true&characterEncoding=utf8&serverTimezone=Asia/Shanghai";
        String username = "root";
        String password = "Hanlongjie0526@";

        FastAutoGenerator.create(url, username, password)
                .globalConfig(builder -> {
                    builder.author("hualingling") // 设置作者
                            .fileOverride() // 覆盖已生成文件w
                            .outputDir("D:\\lingling\\Javaweb\\sports-system\\sports-system-applet\\applet-dal\\src\\main\\java"); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent("com.example.dal.company")
                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml, "D:\\lingling\\Javaweb\\sports-system\\sports-system-applet\\applet-dal\\src\\main\\resources\\mapper")); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    // 设置需要生成的表名
                    builder.addInclude("user_info");
                    builder.entityBuilder().enableLombok();
                    builder.mapperBuilder().enableBaseResultMap().enableMapperAnnotation();
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();
    }
}