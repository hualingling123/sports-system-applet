package com.example.dal.domain;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author hualingling
 * @since 2023-06-28
 */
@Getter
@Setter
public class Team implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 组id
     */
    @TableId(value = "group_id", type = IdType.AUTO)
    private Integer groupId;

    /**
     * 组名
     */
    private String groupName;

    /**
     * 组人数
     */
    private Integer number;

    /**
     * 是否删除
     */
    @TableLogic//逻辑删除
    private Boolean isDelete;


    //字段添加填充内容
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;


    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
