package com.example.dal.domain;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@Getter
@Setter
@TableName("admin_feedback")
public class AdminFeedback implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 反馈标题
     */
    private String feedbackpersonName;

    /**
     * 反馈内容
     */
    private String feedbackContent;

    /**
     * 院校负责人ID（裁判id）
     */
    private Integer feedbackpersonId;

    /**
     * 反馈人名字
     */
    private String feedbackName;

    /**
     * 所属学校（裁判）
     */
    private String school;


    @TableLogic//逻辑删除
    private Boolean isDelete;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
