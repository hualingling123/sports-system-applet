package com.example.dal.domain;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@Getter
@Setter
@TableName("referee_grade")
public class RefereeGrade implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 裁判id
     */
    private String refereeId;

    /**
     * 裁判名字
     */
    private String refereeName;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 用户名字
     */
    private String userName;

    /**
     * 分数
     */
    private String score;

    /**
     * 比赛id
     */
    private String competitionId;

    /**
     * 比赛名字
     */
    private String competitionName;

    /**
     * 比赛类型
     */
    private String competitionType;

    /**
     * 是否犯规
     */
    private Boolean foul;

    /**
     * 犯规内容
     */
    private String foulText;

    /**
     * 惩罚内容
     */
    private String fixText;

    //字段添加填充内容
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
