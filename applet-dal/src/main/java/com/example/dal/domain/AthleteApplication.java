package com.example.dal.domain;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@Getter
@Setter
@TableName("athlete_application")
public class AthleteApplication implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 用户名字
     */
    private String name;

    /**
     * 申请材料
     */
    private String applicationGoods;

    /**
     * 申请理由
     */
    private String applicationReason;

    /**
     * 学校
     */
    private String userSchool;

    /**
     * 比赛id
     */
    private Integer competitionId;

    /**
     * 是否删除
     */
    @TableLogic
    private Boolean isDelete;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
