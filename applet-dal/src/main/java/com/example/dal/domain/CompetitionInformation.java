package com.example.dal.domain;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@Getter
@Setter
@TableName("competition_information")
public class CompetitionInformation implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 比赛ID
     */
    private Integer competitionId;

    /**
     * 比赛名称
     */
    private String competitionName;

    /**
     * 比赛类型（1, 2, 3）
     */
    private Integer competitionType;

    /**
     * 比赛主办方（负责人）
     */
    private String competitionFunctionary;

    /**
     * 开始时间
     */

    private String startTime;

    /**
     * 结束时间
     */
    private String endTime;

    /**
     * 比赛地点
     */
    private String competitionPlace;

    /**
     * 比赛规则
     */
    private String competitionRule;

    /**
     * 参赛要求
     */
    private String entryRequirements;

    /**
     * 分数设置
     */
    private String scoreSetting;


    @TableLogic//逻辑删除
    private Boolean isDelete;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
