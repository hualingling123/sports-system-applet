package com.example.dal.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author hualingling
 * @since 2023-07-07
 */
@Getter
@Setter
@TableName("competition_scores")
public class CompetitionScores implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    /**
     * 比赛id
     */
    private Integer competitionId;

    /**
     * 学生id
     */
    private String userId;

    /**
     * 学生得分
     */
    private String score;

    /**
     * 学生排名
     */
    private String userRank;

    private Boolean isDelete;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


}
