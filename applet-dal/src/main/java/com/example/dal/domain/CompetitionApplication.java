package com.example.dal.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author hualingling
 * @since 2023-07-03
 */
@Getter
@Setter
@TableName("competition_application")
public class CompetitionApplication implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 申请表id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer  id;

    /**
     * 申请比赛id
     */
    private Integer competitionId;

    /**
     * 申请人id
     */
    private Integer userId;

    /**
     * 申请人姓名
     */
    private String userName;

    /**
     * 是否删除
     */
    private Boolean isDelet;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime deleteTime;


}
