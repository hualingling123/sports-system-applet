package com.example.dal.domain;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@Getter
@Setter
@TableName("check_in")
public class CheckIn implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 检录编号
     */
    private Integer checkInId;

    /**
     * 学生ID
     */
    private String userId;

    /**
     * 学生姓名
     */
    private String userName;

    /**
     * 学生所属学校
     */
    private String studentSchool;

    /**
     * 负责检录裁判ID
     */
    private String refereeId;

    /**
     * 比赛ID
     */
    private String competitionId;

    /**
     * 比赛名称
     */
    private String competitionName;

    /**
     * 是否删除
     */
    @TableLogic//逻辑删除
    private Boolean isDelete;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
