package com.example.dal.domain;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@Getter
@Setter
@TableName("audit_record")
public class AuditRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 申请id
     */
    private Integer applicationId;

    /**
     * 通过状态，通过/不通过
     */
    private Integer status;

    /**
     * 审批人id
     */
    private String auditorId;

    /**
     * 审批人名字
     */
    private String auditorName;

    /**
     * 审批时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date auditTime;

    /**
     * 是否删除
     */
    @TableLogic//逻辑删除
    private Boolean isDelete;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
