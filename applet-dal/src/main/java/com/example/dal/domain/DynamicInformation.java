package com.example.dal.domain;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@Getter
@Setter
@TableName("dynamic_information")
public class DynamicInformation implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 动态信息id
     */

    private String informationId;

    /**
     * 动态信息标题
     */
    private String informationName;

    /**
     * 动态信息内容
     */
    private String informationText;

    /**
     * 作者
     */
    private String informationAuthor;

    /**
     * 宣传图
     */
    private String informationImg;


    @TableLogic
    private Boolean isDelete;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.UPDATE)
    private Date updateTime;

}
