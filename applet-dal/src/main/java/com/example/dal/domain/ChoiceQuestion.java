package com.example.dal.domain;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author hualingling
 * @since 2023-06-27
 */
@Getter
@Setter
@TableName("choice_question")
public class ChoiceQuestion implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 章节ID
     */
    private Integer questionId;

    /**
     * 题目编号
     */
    private String title;

    /**
     * 内容
     */
    private String content;

    /**
     * A
     */
    private String optionA;

    /**
     * B
     */
    private String optionB;

    /**
     * C
     */
    private String optionC;

    /**
     * D
     */
    private String optionD;

    /**
     * 答案
     */
    private String correctAnswer;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
