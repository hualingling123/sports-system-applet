package com.example.dal.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeamScore {
    private int groupId;
    private String groupName;
    private int score;
}
