package com.example.dal.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@Getter
@Setter
@TableName("competition_rank")
public class CompetitionRank implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    /**
     * 比赛id
     */
    private Integer compertitionId;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 用户排名
     */
    private String rank;

    /**
     * 学校排名
     */
    private String schoolRank;

    /**
     * 是否删除
     */
    @TableLogic//逻辑删除
    private Boolean isDelete;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;



}
