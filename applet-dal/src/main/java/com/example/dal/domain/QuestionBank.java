package com.example.dal.domain;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@Getter
@Setter
@TableName("question_bank")
public class QuestionBank implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 章节id
     */
    private Integer questionId;

    /**
     * 章节名字
     */
    private String name;

    /**
     * 作者
     */
    private String author;

    @TableLogic//逻辑删除
    private Boolean isDelete;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
