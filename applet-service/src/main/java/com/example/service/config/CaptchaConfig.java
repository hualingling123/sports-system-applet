package com.example.service.config;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Properties;

/**
* 验证码
* @author:  hualingling
*/
@Component
public class CaptchaConfig {
    @Bean
    public DefaultKaptcha getDefaultCaptcha(){
        DefaultKaptcha defaultKaptcha = new DefaultKaptcha();
        Properties properties = new Properties();
        properties.setProperty("captcha.border", "no");
        properties.setProperty("captcha.border.color", "105,179,90");
        properties.setProperty("captcha.text-producer.font.color", "black");
        properties.setProperty("captcha.image.width", "125");
        properties.setProperty("captcha.image.height", "45");
        properties.setProperty("captcha.text-producer.font.size", "35");
        properties.setProperty("captcha.text-producer.char.length", "4");
        properties.setProperty("captcha.text-producer.font.names", "宋体,楷体,微软雅黑");
        Config config = new Config(properties);
        defaultKaptcha.setConfig(config);
        return defaultKaptcha;
    }
}