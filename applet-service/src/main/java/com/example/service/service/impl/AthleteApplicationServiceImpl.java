package com.example.service.service.impl;

import com.example.dal.domain.AthleteApplication;
import com.example.dal.mapper.AthleteApplicationMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.service.service.AthleteApplicationService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@Service
public class AthleteApplicationServiceImpl extends ServiceImpl<AthleteApplicationMapper, AthleteApplication> implements AthleteApplicationService {

}
