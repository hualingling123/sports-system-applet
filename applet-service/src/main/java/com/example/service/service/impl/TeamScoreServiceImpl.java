package com.example.service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.dal.domain.TeamScore;
import com.example.dal.mapper.TeamScoreMapper;
import com.example.service.service.TeamScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeamScoreServiceImpl extends ServiceImpl<TeamScoreMapper, TeamScore> implements TeamScoreService {
    @Autowired
    TeamScoreMapper teamScoreMapper;



    @Override
    public List<TeamScore> teamRank(int id) {
        return teamScoreMapper.teamRank(id);
    }
}
