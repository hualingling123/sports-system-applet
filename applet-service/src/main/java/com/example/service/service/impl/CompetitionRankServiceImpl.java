package com.example.service.service.impl;

import com.example.dal.domain.CompetitionRank;
import com.example.dal.mapper.CompetitionRankMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.service.service.CompetitionRankService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@Service
public class CompetitionRankServiceImpl extends ServiceImpl<CompetitionRankMapper, CompetitionRank> implements CompetitionRankService {

}
