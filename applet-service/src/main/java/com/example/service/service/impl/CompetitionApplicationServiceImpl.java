package com.example.service.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.dal.domain.CompetitionApplication;
import com.example.dal.domain.CompetitionInformation;
import com.example.dal.domain.UserInfo;
import com.example.dal.mapper.CompetitionApplicationMapper;
import com.example.dal.mapper.CompetitionInformationMapper;
import com.example.dal.mapper.UserInfoMapper;
import com.example.service.domain.vo.UserVo;
import com.example.service.service.CompetitionApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hualingling
 * @since 2023-07-03
 */
@Service
public class CompetitionApplicationServiceImpl extends ServiceImpl<CompetitionApplicationMapper, CompetitionApplication> implements CompetitionApplicationService {
    @Autowired
    CompetitionApplicationMapper competitionApplicationMapper;
    @Autowired
    CompetitionInformationMapper competitionInformationMapper;
    @Autowired
    UserInfoMapper userInfoMapper;


    @Override
     public boolean joinCompetition(CompetitionInformation competitionInformation, UserVo userVo) {
        QueryWrapper<CompetitionInformation> qw = new QueryWrapper<>();
        qw.eq("competition_id",competitionInformation.getCompetitionId());
        if(competitionInformationMapper.selectOne(qw)==null){
            return false;
        }

        QueryWrapper<UserInfo> qw1 = new QueryWrapper<>();
        qw1.eq("user_id",userVo.getUserId());
        UserInfo user = userInfoMapper.selectOne(qw1);
        if(user.getSports()==0){
            return false;
        }

        System.out.println(1);

        QueryWrapper<CompetitionApplication> qw2 = new QueryWrapper<>();
        qw2.eq("user_id",userVo.getUserId()).eq("competition_id",competitionInformation.getCompetitionId());
        if(competitionApplicationMapper.selectList(qw2).size()!=0){
            System.out.println(competitionApplicationMapper.selectList(qw2));
            return false;
        }


        CompetitionApplication competitionApplication = new CompetitionApplication();
        competitionApplication.setCompetitionId(competitionInformation.getCompetitionId());
        competitionApplication.setId(null);
        competitionApplication.setUserId(Integer.parseInt(userVo.getUserId()));
        competitionApplication.setUserName(user.getUserName());
        competitionApplication.setIsDelet(false);
        competitionApplicationMapper.insert(competitionApplication);

        return true;
    }
}
