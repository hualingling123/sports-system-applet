package com.example.service.service.impl;

import com.example.dal.domain.ChoiceQuestion;
import com.example.dal.mapper.ChoiceQuestionMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.service.service.ChoiceQuestionService;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@Service
public class ChoiceQuestionServiceImpl extends ServiceImpl<ChoiceQuestionMapper, ChoiceQuestion> implements ChoiceQuestionService {

    /**
     * 从集合中随机取出N个不重复的元素
     * @param list 需要被取出数据的集合
     * @param n 取出的元素数量
     * @return
     */
    public List<Integer> createRandoms(List<Integer> list, int n) {
        Map<Integer,String> map = new HashMap();
        List<Integer> news = new ArrayList();
        if (list.size() <= n) {
            return list;
        } else {
            while (map.size() < n) {
                int random = (int)(Math.random() * list.size());
                if (!map.containsKey(random)) {
                    map.put(random, "");
                    news.add(list.get(random));
                }
            }
            return news;
        }
    }



    @Override
    public  List<Integer> generateUniqueLists(List<Integer> inputList, int n) {
        if (inputList.size() < n) {
            throw new IllegalArgumentException("Input list size is smaller than the required number of unique lists.");
        }

        List<Integer> resultList = new ArrayList<>();
        List<Integer> tempList = new ArrayList<>(inputList);
        Random random = new Random();

        for (int i = 0; i < n; i++) {
            int remainingElements = tempList.size();
            if (remainingElements == 0) {
                // Reset tempList if all elements have been used
                tempList = new ArrayList<>(inputList);
                remainingElements = tempList.size();
            }

            int randomIndex = random.nextInt(remainingElements);
            int element = tempList.get(randomIndex);
            tempList.remove(randomIndex);
            resultList.add(element);
        }

        return resultList;
    }


}
