package com.example.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.dal.domain.UserInfo;
import com.example.service.constants.Result;
import com.example.service.domain.vo.PageParam;
import com.example.service.domain.vo.UserVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */

public interface UserInfoService extends IService<UserInfo> {


    /**
     * 用户id与密码
     * @param userid
     * @param password
     * @return
     */
    UserInfo findUser(String userid, String password);

    /**
     * 根据账户查找用户
     * @param userId
     * @return
     */
    UserInfo findUserByUserId(String userId);


    UserVo findUserVoById(Long id);


    UserInfo findUserById(Long id);

    /**
     * 根据token查询用户信息
     * @param token
     * @return
     */
    Result findUserByToken(String token);

    /**
    * 用户信息
    */
    Result findUserInfo(PageParam pageParam);

}

