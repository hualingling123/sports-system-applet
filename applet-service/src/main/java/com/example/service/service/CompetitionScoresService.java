package com.example.service.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.example.dal.domain.CompetitionScores;
import com.example.service.domain.vo.UserVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hualingling
 * @since 2023-07-07
 */
public interface CompetitionScoresService extends IService<CompetitionScores> {
    List<CompetitionScores> getScore(UserVo userVo);
}
