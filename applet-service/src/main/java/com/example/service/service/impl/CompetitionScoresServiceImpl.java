package com.example.service.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.dal.domain.CompetitionScores;
import com.example.dal.domain.UserInfo;
import com.example.dal.mapper.CompetitionScoresMapper;
import com.example.dal.mapper.UserInfoMapper;
import com.example.service.domain.vo.UserVo;
import com.example.service.service.CompetitionScoresService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hualingling
 * @since 2023-07-07
 */
@Service
public class CompetitionScoresServiceImpl extends ServiceImpl<CompetitionScoresMapper, CompetitionScores> implements CompetitionScoresService {
    @Autowired
    CompetitionScoresMapper competitionScoresMapper;
    @Autowired
    UserInfoMapper userInfoMapper;
    @Override
    public List<CompetitionScores> getScore(UserVo userVo) {
        QueryWrapper<UserInfo> qw = new QueryWrapper<>();
        qw.eq("user_id",userVo.getUserId());

        if (userInfoMapper.selectOne(qw).getSports()==0){
            return null;
        }

        QueryWrapper<CompetitionScores> qw1= new QueryWrapper<>();
        qw1.eq("user_id",userVo.getUserId());

        return competitionScoresMapper.selectList(qw1);
    }
}
