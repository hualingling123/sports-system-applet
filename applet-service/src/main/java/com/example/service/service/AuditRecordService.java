package com.example.service.service;

import com.example.dal.domain.AuditRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
public interface AuditRecordService extends IService<AuditRecord> {

}
