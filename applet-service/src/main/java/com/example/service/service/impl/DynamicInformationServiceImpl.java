package com.example.service.service.impl;

import com.example.dal.domain.DynamicInformation;
import com.example.dal.mapper.DynamicInformationMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.service.service.DynamicInformationService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@Service
public class DynamicInformationServiceImpl extends ServiceImpl<DynamicInformationMapper, DynamicInformation> implements DynamicInformationService {

}
