package com.example.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.dal.domain.CompetitionApplication;
import com.example.dal.domain.CompetitionInformation;
import com.example.dal.domain.UserInfo;
import com.example.service.domain.vo.UserVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hualingling
 * @since 2023-07-03
 */
public interface CompetitionApplicationService extends IService<CompetitionApplication> {
    boolean joinCompetition(CompetitionInformation competitionInformation, UserVo userVo);
}
