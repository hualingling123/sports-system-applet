package com.example.service.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.dal.domain.UserInfo;
import com.example.dal.mapper.UserInfoMapper;
import com.example.service.constants.ErrorCode;
import com.example.service.constants.Result;
import com.example.service.domain.vo.LoginUserVo;
import com.example.service.domain.vo.PageParam;
import com.example.service.domain.vo.PageResult;
import com.example.service.domain.vo.UserVo;
import com.example.service.service.LoginService;
import com.example.service.service.UserInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements UserInfoService {

    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    @Lazy
    @Autowired
    private LoginService loginService;

    @Override
    public UserVo findUserVoById(Long id) {
        UserInfo userInfo = userInfoMapper.selectById(id);
        if (userInfo == null) {
            userInfo = new UserInfo();
            userInfo.setId(1l);
            userInfo.setUserName("hualingling");
        }
        UserVo userVo = new UserVo();
        BeanUtils.copyProperties(userInfo, userVo);
        userVo.setUserId(String.valueOf(userInfo.getId()));
        return userVo;
    }

    @Override
    public UserInfo findUserById(Long id) {
        UserInfo userInfo = userInfoMapper.selectById(id);
        if (userInfo == null) {
            userInfo = new UserInfo();
            userInfo.setUserName("hualingling");
        }
        return userInfo;
    }

    @Override
    public UserInfo findUser(String userId, String password) {
        LambdaQueryWrapper<UserInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserInfo::getUserId, userId);
        queryWrapper.eq(UserInfo::getPassword, password);
        queryWrapper.select(UserInfo::getUserId, UserInfo::getId, UserInfo::getUserName);
        queryWrapper.last("limit 1");

        return userInfoMapper.selectOne(queryWrapper);
    }

    @Override
    public UserInfo findUserByUserId(String userId) {
        LambdaQueryWrapper<UserInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserInfo::getUserId, userId);
        queryWrapper.last("limit 1");
        return this.userInfoMapper.selectOne(queryWrapper);
    }

    @Override
    public Result findUserByToken(String token) {
        /**
         * 1. token合法性校验
         *    是否为空，解析是否成功 redis是否存在
         * 2. 如果校验失败 返回错误
         * 3. 如果成功，返回对应的结果 LoginUserVo
         */
        UserInfo userInfo = loginService.checkToken(token);
        if (userInfo == null) {
            return Result.fail(ErrorCode.TOKEN_ERROR.getCode(), ErrorCode.TOKEN_ERROR.getMsg());
        }
        LoginUserVo loginUserVo = new LoginUserVo();
        loginUserVo.setId(String.valueOf(userInfo.getId()));
        loginUserVo.setUserName(userInfo.getUserName());
        loginUserVo.setUserSchool(userInfo.getUserSchool());
        loginUserVo.setUserId(userInfo.getUserId());
        return Result.success(loginUserVo);
    }

    @Override
    public Result findUserInfo(PageParam pageParam) {
        Page<UserInfo> page = new Page<>(pageParam.getCurrentPage(),pageParam.getPageSize());
        LambdaQueryWrapper<UserInfo> queryWrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(pageParam.getQueryString())){
            queryWrapper.eq(UserInfo::getUserId,pageParam.getQueryString());
        }
        Page<UserInfo> permissionPage = userInfoMapper.selectPage(page, queryWrapper);
        PageResult<UserInfo> pageResult = new PageResult<>();
        pageResult.setList(permissionPage.getRecords());
        pageResult.setTotal(permissionPage.getTotal());
        return Result.success(pageResult);
    }

}