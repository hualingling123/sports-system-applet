package com.example.service.service.impl;

import com.example.dal.domain.CompetitionInformation;
import com.example.dal.mapper.CompetitionInformationMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.service.service.CompetitionInformationService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@Service
public class CompetitionInformationServiceImpl extends ServiceImpl<CompetitionInformationMapper, CompetitionInformation> implements CompetitionInformationService {

}
