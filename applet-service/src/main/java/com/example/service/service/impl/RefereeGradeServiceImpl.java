package com.example.service.service.impl;

import com.example.dal.domain.RefereeGrade;
import com.example.dal.mapper.RefereeGradeMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.service.service.RefereeGradeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@Service
public class RefereeGradeServiceImpl extends ServiceImpl<RefereeGradeMapper, RefereeGrade> implements RefereeGradeService {

}
