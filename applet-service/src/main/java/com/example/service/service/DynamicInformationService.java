package com.example.service.service;

import com.example.dal.domain.DynamicInformation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
public interface DynamicInformationService extends IService<DynamicInformation> {

}
