package com.example.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.dal.domain.ScoreFeedback;
import com.example.dal.domain.TeamScore;

import java.util.List;

public interface TeamScoreService  extends IService<TeamScore> {
    List<TeamScore> teamRank(int id);
}
