package com.example.service.service.impl;

import com.example.dal.domain.AdminFeedback;
import com.example.dal.mapper.AdminFeedbackMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.service.service.AdminFeedbackService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@Service
public class AdminFeedbackServiceImpl extends ServiceImpl<AdminFeedbackMapper, AdminFeedback> implements AdminFeedbackService {

}
