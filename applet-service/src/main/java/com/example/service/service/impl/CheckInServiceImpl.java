package com.example.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.dal.domain.CheckIn;
import com.example.dal.domain.CompetitionApplication;
import com.example.dal.domain.UserInfo;
import com.example.dal.mapper.CheckInMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.dal.mapper.CompetitionApplicationMapper;
import com.example.dal.mapper.UserInfoMapper;
import com.example.service.domain.vo.UserVo;
import com.example.service.service.CheckInService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jws.soap.SOAPBinding;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@Service
public class CheckInServiceImpl extends ServiceImpl<CheckInMapper, CheckIn> implements CheckInService {
    @Autowired
    CompetitionApplicationMapper competitionApplicationMapper;
    @Autowired
    UserInfoMapper userInfoMapper;
/*    传入参数
    {
        "userVo":{
            "userid":1,
            "username":"张三"
        },
        "checkIn":{
            "competitionId":1
        }
    }*/

    @Override
    public boolean checkIn(UserVo userVo,CheckIn checkIn) {
        QueryWrapper<CompetitionApplication> qw = new QueryWrapper<>();
        qw.eq("user_id",userVo.getUserId());
        qw.eq("competition_id",checkIn.getCompetitionId());

        QueryWrapper<UserInfo> qw1 = new QueryWrapper<>();
        qw1.eq("user_id",userVo.getUserId());

        if (userInfoMapper.selectOne(qw1).getSports()==0){
            return false;
        }


        if(competitionApplicationMapper.selectList(qw).size()==1){
            return true;
        }


        return false;
    }
}
