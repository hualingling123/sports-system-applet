package com.example.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.dal.domain.Role;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
public interface RoleService extends IService<Role> {

}
