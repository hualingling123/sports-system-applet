package com.example.service.service;

import com.example.dal.domain.ChoiceQuestion;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
public interface ChoiceQuestionService extends IService<ChoiceQuestion> {

    public List<Integer> createRandoms(List<Integer> list, int n);

    public  List<Integer> generateUniqueLists(List<Integer> inputList, int n);
}
