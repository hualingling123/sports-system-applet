package com.example.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.dal.domain.AthleteApplication;
import com.example.dal.domain.AuditRecord;
import com.example.dal.domain.UserInfo;
import com.example.service.domain.vo.UserVo;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Transactional
public interface CollegeAddSportsService extends IService<UserInfo> {
    boolean AddSports(AthleteApplication athleteApplication,AuditRecord auditRecord);
    List<AthleteApplication> getList();
    AthleteApplication queryStu(UserVo userVo);
}
