package com.example.service.service;

import com.example.dal.domain.CheckIn;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.dal.domain.UserInfo;
import com.example.service.domain.vo.UserVo;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
public interface CheckInService extends IService<CheckIn> {
    boolean checkIn(UserVo userVo,CheckIn checkIn);
}
