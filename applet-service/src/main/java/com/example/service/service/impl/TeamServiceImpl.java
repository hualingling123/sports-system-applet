package com.example.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.dal.domain.Team;
import com.example.dal.domain.UserInfo;
import com.example.dal.mapper.TeamMapper;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.dal.mapper.UserInfoMapper;
import com.example.service.domain.vo.UserVo;
import com.example.service.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hualingling
 * @since 2023-06-28
 */
@Service
public class TeamServiceImpl extends ServiceImpl<TeamMapper, Team> implements TeamService {
    @Autowired
    TeamMapper teamMapper;
    @Autowired
    UserInfoMapper userInfoMapper;
    @Override
    public List<Team> teamList() {
        QueryWrapper<Team> queryWrapper = new QueryWrapper<>();
        List<Team> team = teamMapper.selectList(queryWrapper);
        return team;
    }

    @Override
    public boolean joinTeam(Team team, UserVo userVo) {
        QueryWrapper<UserInfo> userInfoQueryWrapper = new QueryWrapper<>();
        userInfoQueryWrapper.eq("user_id",userVo.getUserId());
        UserInfo user = userInfoMapper.selectOne(userInfoQueryWrapper);

        QueryWrapper<Team> qw = new QueryWrapper<>();
        qw.eq("group_id",team.getGroupId());


        if(user==null||user.getSports()==0||teamMapper.selectList(qw).size()==0){
            return false;
        }


        if(user.getGroupId()==null||user.getGroupId()<=0){
            teamMapper.joinTeam(userVo.getUserId(),team.getGroupId());
            teamMapper.changeTeamNum(team.getGroupId());
            return true;
        }

        return false;
    }

    @Override
    public boolean quitTeam(UserVo userVo) {
        QueryWrapper<UserInfo> userInfoQueryWrapper = new QueryWrapper<>();
        userInfoQueryWrapper.eq("user_id",userVo.getUserId());
        UserInfo user = userInfoMapper.selectOne(userInfoQueryWrapper);

        QueryWrapper<Team> teamQueryWrapper = new QueryWrapper<>();
        teamQueryWrapper.eq("group_id",user.getGroupId());

        if(user.getGroupId()!=null&&user.getGroupId()>0){
            teamMapper.changeTeamNum1(user.getGroupId());
            teamMapper.quitTeam(userVo.getUserId());

            Team teams  = teamMapper.selectOne(teamQueryWrapper);
            if(teams.getNumber()<=0){
                teamMapper.delete(teamQueryWrapper);
            }

            return true;
        }

        return false;
    }



    @Override
    public boolean createTeam(Team team) {
        QueryWrapper<Team> qw = new QueryWrapper<>();
        qw.eq("group_name",team.getGroupName());
        if (teamMapper.selectList(qw).size()>0){
            return false;
        }

        return teamMapper.insert(team)>=1;
    }

}
