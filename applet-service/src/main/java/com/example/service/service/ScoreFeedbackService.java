package com.example.service.service;

import com.example.dal.domain.ScoreFeedback;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
public interface ScoreFeedbackService extends IService<ScoreFeedback> {

}
