package com.example.service.service.impl;

import com.example.dal.domain.ScoreFeedback;
import com.example.dal.mapper.ScoreFeedbackMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.service.service.ScoreFeedbackService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@Service
public class ScoreFeedbackServiceImpl extends ServiceImpl<ScoreFeedbackMapper, ScoreFeedback> implements ScoreFeedbackService {

}
