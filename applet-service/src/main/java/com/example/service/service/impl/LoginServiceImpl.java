package com.example.service.service.impl;

import com.alibaba.fastjson.JSON;
import com.example.dal.domain.UserInfo;
import com.example.service.constants.ErrorCode;
import com.example.service.constants.Result;
import com.example.service.domain.vo.LoginParam;
import com.example.service.service.LoginService;
import com.example.service.service.UserInfoService;
import com.example.service.utils.JWTUtils;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
@Transactional
public class LoginServiceImpl implements LoginService {

    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    private static final String slat = "lingling!@#";

    @Override
    public Result login(LoginParam loginParam) {
        /**
         * 1. 检查参数是否合法
         * 2. 根据用户名和密码去user表中查询 是否存在
         * 3. 如果不存在 登录失败
         * 4. 如果存在 ，使用jwt 生成token 返回给前端
         * 5. token放入redis当中，redis  token：user信息 设置过期时间
         *  (登录认证的时候 先认证token字符串是否合法，去redis认证是否存在)
         */
        String userid = loginParam.getUserId();
        String password = loginParam.getPassword();
        if (StringUtils.isBlank(userid) || StringUtils.isBlank(password)){
            return Result.fail(ErrorCode.PARAMS_ERROR.getCode(),ErrorCode.PARAMS_ERROR.getMsg());
        }
        password = DigestUtils.md5Hex(password + slat);
        UserInfo userInfo = userInfoService.findUser(userid,password);
        if (userInfo == null){
            return Result.fail(ErrorCode.ACCOUNT_PWD_NOT_EXIST.getCode(),ErrorCode.ACCOUNT_PWD_NOT_EXIST.getMsg());
        }
        String token = JWTUtils.createToken(userInfo.getId());

        redisTemplate.opsForValue().set("TOKEN_"+token, JSON.toJSONString(userInfo),1, TimeUnit.DAYS);
        return Result.success(token);
    }

    @Override
    public UserInfo checkToken(String token) {
        if (StringUtils.isBlank(token)){
            return null;
        }
        Map<String, Object> stringObjectMap = JWTUtils.checkToken(token);
        if (stringObjectMap == null){
            return null;
        }
        String userJson = redisTemplate.opsForValue().get("TOKEN_" + token);
        if (StringUtils.isBlank(userJson)){
            return null;
        }
        UserInfo sysUser = JSON.parseObject(userJson, UserInfo.class);
        return sysUser;
    }

    @Override
    public Result logout(String token) {
        redisTemplate.delete("TOKEN_"+token);
        return Result.success(null);
    }

    @Override
    public Result register(LoginParam loginParam) {
        /**
         * 1. 判断参数 是否合法
         * 2. 判断账户是否存在，存在 返回账户已经被注册
         * 3. 不存在，注册用户
         * 4. 生成token
         * 5. 存入redis 并返回
         * 6. 注意 加上事务，一旦中间的任何过程出现问题，注册的用户 需要回滚
         */
        String userId = loginParam.getUserId();
        String password = loginParam.getPassword();
        String userName = loginParam.getUserName();
        if (StringUtils.isBlank(userId)
                || StringUtils.isBlank(password)
                || StringUtils.isBlank(userName)
        ){
            return Result.fail(ErrorCode.PARAMS_ERROR.getCode(),ErrorCode.PARAMS_ERROR.getMsg());
        }
        UserInfo userInfo =  userInfoService.findUserByUserId(userId);
        if (userInfo != null){
            return Result.fail(ErrorCode.ACCOUNT_EXIST.getCode(),"账户已经被注册了");
        }
        userInfo = new UserInfo();
        userInfo.setUserName(userName);//用户名
        userInfo.setUserId(userId);//账号
        userInfo.setPassword(DigestUtils.md5Hex(password+slat));//密码
        userInfo.setSalt(DigestUtils.md5Hex(password+slat));
        userInfo.setEmail(userInfo.getEmail());//email
        userInfo.setPhone(userInfo.getPhone());//手机号
        userInfo.setUserSchool(userInfo.getUserSchool());//学校
        userInfo.setAddress(userInfo.getAddress());//地址
        userInfo.setGroupId(userInfo.getGroupId());
        userInfo.setRoleId(userInfo.getRoleId());
        userInfo.setRemark(userInfo.getRemark());//签名
        userInfo.setStatus(userInfo.getStatus());//在线
        userInfo.setSports(userInfo.getSports());//运动员
        userInfo.setIsDelete(userInfo.getIsDelete());//删除
        userInfo.setCreateTime(new Date());
        userInfo.setLastLoginTime(new Date());
        this.userInfoService.save(userInfo);

        String token = JWTUtils.createToken(userInfo.getId());

        redisTemplate.opsForValue().set("TOKEN_"+token, JSON.toJSONString(userInfo),1, TimeUnit.DAYS);
        return Result.success(token);
    }

}