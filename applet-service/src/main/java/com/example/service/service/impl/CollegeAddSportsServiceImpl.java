package com.example.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.dal.domain.AthleteApplication;
import com.example.dal.domain.AuditRecord;
import com.example.dal.domain.UserInfo;
import com.example.dal.mapper.AuditRecordMapper;
import com.example.dal.mapper.CollegeAddSportsMapper;
import com.example.dal.mapper.AthleteApplicationMapper;
import com.example.service.domain.vo.UserVo;
import com.example.service.service.AuditRecordService;
import com.example.service.service.CollegeAddSportsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class CollegeAddSportsServiceImpl extends ServiceImpl<CollegeAddSportsMapper, UserInfo> implements CollegeAddSportsService {
    @Autowired
    CollegeAddSportsMapper collegeAddSportsMapper;
    @Autowired
    AuditRecordMapper auditRecordMapper;
    @Autowired
    AthleteApplicationMapper athleteApplicationMapper;
    @Override
    public boolean AddSports(AthleteApplication athleteApplication,AuditRecord auditRecord) {


        QueryWrapper<UserInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id",athleteApplication.getUserId());
        UserInfo userInfo = collegeAddSportsMapper.selectOne(queryWrapper);
        if (userInfo==null){
            return false;
        }

        if(userInfo.getSports()==0){
            userInfo.setSports(1);
            collegeAddSportsMapper.updateById(userInfo);
            auditRecordMapper.insert(auditRecord);
            return true;
        }else {
            return false;
        }
    }
    @Override
    public List<AthleteApplication> getList() {
        QueryWrapper<AthleteApplication> qw = new QueryWrapper<>();
        return athleteApplicationMapper.selectList(qw);
    }

    @Override
    public AthleteApplication queryStu(UserVo userVo) {
        QueryWrapper<AthleteApplication> qw = new QueryWrapper<>();
        qw.eq("user_id",userVo.getUserId());
        return athleteApplicationMapper.selectOne(qw);
    }
}
