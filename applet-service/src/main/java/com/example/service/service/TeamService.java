package com.example.service.service;

import com.example.dal.domain.Team;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.service.domain.vo.UserVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hualingling
 * @since 2023-06-28
 */
public interface TeamService extends IService<Team> {
    List<Team> teamList();

    boolean joinTeam(Team team, UserVo userVo);
    boolean quitTeam(UserVo userVo);

    boolean createTeam(Team team);
}
