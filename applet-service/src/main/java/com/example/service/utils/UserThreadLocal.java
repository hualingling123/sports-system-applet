package com.example.service.utils;


import com.example.dal.domain.UserInfo;

public class UserThreadLocal {

    private UserThreadLocal(){}
    //线程变量隔离
    private static final ThreadLocal<UserInfo> LOCAL = new ThreadLocal<>();

    public static void put(UserInfo sysUser){
        LOCAL.set(sysUser);
    }

    public static UserInfo get(){
        return LOCAL.get();
    }

    public static void remove(){
        LOCAL.remove();
    }
}
