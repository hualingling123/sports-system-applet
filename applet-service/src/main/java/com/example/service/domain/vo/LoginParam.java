package com.example.service.domain.vo;

import lombok.Data;

@Data
public class LoginParam {

    private String userId;

    private String password;

    private String userName;

}
