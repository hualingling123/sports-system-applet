package com.example.service.domain.vo;

import lombok.Data;

@Data
public class UserVo {

    private String id;

    private String userName;

    private String userId;

    private String school;

    private String address;

    private String sports;

    private String Email;



}

