package com.example.service.domain.vo;

import lombok.Data;

@Data
public class LoginUserVo {

    private String id;

    private String userId;

    private String userName;

    private String userSchool;
}
