package com.example.controller;

import com.example.dal.domain.AthleteApplication;
import com.example.dal.domain.AuditRecord;
import com.example.service.constants.Result;
import com.example.service.domain.vo.UserVo;
import com.example.service.service.CollegeAddSportsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/collegeAdd")
public class CollegeAddSportsController {
    @Autowired
    CollegeAddSportsService collegeAddSportsService;

    /* 添加运动员功能，用于审核后将数据库内对应的id用户的运动员字段改为true
    { 传入参数
        "userVo"(这个是当前账号的用户信息,目前在该功能是属于院校的账号信息):
        {
                "userId":2,
                "userName":"张三"
        },
        "athleteApplication"(添加运动员是基于运动员申请表里面的userid 所以请将被审核人的userid通过athleteApplication传回来):
        {
                "userId":2
        }
    }
    */
    @PutMapping("/application/add")
    public Result addAthlete(@RequestBody Map<String,Object> map){
            try{
                ObjectMapper objectMapper=new ObjectMapper();
                AthleteApplication athleteApplication = objectMapper.readValue(objectMapper.writeValueAsString(map.get("athleteApplication")), AthleteApplication.class);
                UserVo userVo = objectMapper.readValue(objectMapper.writeValueAsString(map.get("userVo")), UserVo.class);

                AuditRecord auditRecord = new AuditRecord();
                auditRecord.setAuditorId((userVo.getUserId()));
                auditRecord.setAuditorName(userVo.getUserName());
                auditRecord.setIsDelete(false);
                auditRecord.setApplicationId(athleteApplication.getId());
                auditRecord.setStatus(1);

                boolean save = collegeAddSportsService.AddSports(athleteApplication,auditRecord);

                return Result.success("修改为运动员结果为："+save);
            }catch (Exception e){
                return Result.fail(500,"系统出现异常");
            }
        }

        //获取申请成为运动员的学生名单,不需要参数直接调用即可
        @GetMapping("/application")
        public List<AthleteApplication> StuApplications(){
            return collegeAddSportsService.getList();
        }

        //查询该审核学生的申请材料
        /*   传入参数
        {
            "userId":1
        }
        */
        @GetMapping("/application/stu")
        public AthleteApplication stuApplication(@RequestBody UserVo userVo){
            return collegeAddSportsService.queryStu(userVo);
        }
    }


