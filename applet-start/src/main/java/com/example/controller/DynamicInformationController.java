package com.example.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.dal.domain.DynamicInformation;
import com.example.service.constants.Result;
import com.example.service.service.DynamicInformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 赛事动态消息
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@RestController
@RequestMapping("/dynamic-information")
public class DynamicInformationController {
    @Autowired
    DynamicInformationService dynamicInformationService;
    @RequestMapping("/user/DynamicInformationController")
    public Result getUserDynamicInformationController(){
        List<DynamicInformation> list = dynamicInformationService.list(null);
        return Result.success(list);
    }
}
