package com.example.controller;


import com.example.dal.domain.CompetitionInformation;
import com.example.service.constants.Result;
import com.example.service.service.CompetitionInformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *比赛信息
 * @author hualingling
 * @since 2023-06-25
 */
@RestController
@RequestMapping("/competition-information")
public class CompetitionInformationController {
    @Autowired
    CompetitionInformationService competitionInformationService;

    @RequestMapping("/selectAllCompetitionInformation")
    public Result selectAllCompetitionInformation(){
        try{
            List<CompetitionInformation> list = competitionInformationService.list(null);
            return Result.success(list);
        }catch (Exception e){
            return Result.fail(500,"系统异常");
        }

    }

}
