package com.example.controller;


import com.example.dal.domain.AthleteApplication;
import com.example.dal.domain.AuditRecord;
import com.example.dal.domain.CheckIn;
import com.example.service.constants.Result;
import com.example.service.domain.vo.UserVo;
import com.example.service.service.CheckInService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@RestController
@RequestMapping("/check-in")
public class CheckInController {
    @Autowired
    CheckInService checkInService;

    /* 传入参数
    {
        "userVo":{
            "userId":1,
            "userName":"张三"
        },
        "checkIn":{
            "competitionId":1

        }
    }
    */
    @GetMapping("/check")
    public Result checkIn(@RequestBody Map<String,Object> map){
        try{
            ObjectMapper objectMapper=new ObjectMapper();
            CheckIn checkIn = objectMapper.readValue(objectMapper.writeValueAsString(map.get("checkIn")), CheckIn.class);
            UserVo userVo = objectMapper.readValue(objectMapper.writeValueAsString(map.get("userVo")), UserVo.class);

            boolean result = checkInService.checkIn(userVo,checkIn);

            return Result.success("检录成功:"+result);
        }catch (Exception e){
            return Result.fail(500,"系统出现异常"+e);
        }
    }
}
