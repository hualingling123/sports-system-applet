package com.example.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.dal.domain.UserInfo;
import com.example.service.constants.Result;
import com.example.service.domain.vo.PageParam;
import com.example.service.domain.vo.UserVo;
import com.example.service.service.UserInfoService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@RestController
@RequestMapping("/user-info")
public class UserInfoController {

        @Autowired
        private UserInfoService userInfoService;

        ///users/currentUser
        @GetMapping("/currentUser")
        public Result currentUser(String token){
            return userInfoService.findUserByToken(token);
        }

         //user/find
         @PostMapping("/userList")
         public Result listUser(PageParam pageParam){
             return userInfoService.findUserInfo(pageParam);
         }


    /**
     * 修改个人信息
     *
     * 传入参数
     * {
     *     "userVo":{
     *         "userId":"1"
     *     },
     *     "userInfo":{
     *         "password":"44441",
     *         "phone":"123455",
     *         "email":"@qq.com",
     *         "userSchool":"广西职业技术学院",
     *         "remark":"我是第一",
     *         "address":"广西南宁"
     *     }
     *
     * }
     */
    @RequestMapping("/user/updateUserInfo")
    public Result updateUserInfo(@RequestBody Map<String,Object> map) throws JsonProcessingException {
  try{
      ObjectMapper objectMapper=new ObjectMapper();
      UserVo userVo = objectMapper.readValue(objectMapper.writeValueAsString(map.get("userVo")), UserVo.class);
      UserInfo userInfo = objectMapper.readValue(objectMapper.writeValueAsString(map.get("userInfo")), UserInfo.class);
      LambdaQueryWrapper<UserInfo> lambdaQueryWrapper=new LambdaQueryWrapper<>();
      lambdaQueryWrapper.eq(UserInfo::getUserId,userVo.getUserId());
      UserInfo userInfo1=new UserInfo();
      userInfo1.setPhone(userInfo.getPhone());
      userInfo1.setEmail(userInfo.getEmail());
      userInfo1.setAddress(userInfo.getAddress());
      userInfo1.setPassword(userInfo.getPassword());
      userInfo1.setUserSchool(userInfo.getUserSchool());
      userInfo1.setRemark(userInfo.getRemark());
      boolean update = userInfoService.update(userInfo1, lambdaQueryWrapper);
      return Result.success("修改"+update);
  }catch (Exception e){
      return Result.fail(500,"出现异常");
  }

    }

}
