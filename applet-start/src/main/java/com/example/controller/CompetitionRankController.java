package com.example.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.dal.domain.RefereeGrade;
import com.example.dal.domain.UserInfo;
import com.example.service.constants.Result;
import com.example.service.service.RefereeGradeService;
import com.example.service.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *比赛排名表
 * @author hualingling
 * @since 2023-06-25
 */
@RestController
@RequestMapping("/competition-rank")
public class CompetitionRankController {


    @Autowired
    RefereeGradeService refereeGradeService;
    /**
     * 个人排名(不区分比赛进行排名)
     */
    @RequestMapping("/personcompetitionRank")
    public Result personcompetitionRank(){
        LambdaQueryWrapper<RefereeGrade> lambdaQueryWrapper=new LambdaQueryWrapper<>();
        lambdaQueryWrapper.orderByDesc(RefereeGrade::getScore);
        List<RefereeGrade> list = refereeGradeService.list(lambdaQueryWrapper);
        return Result.success(list);
    }



}
