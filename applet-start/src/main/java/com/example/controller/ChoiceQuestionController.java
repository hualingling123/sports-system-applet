package com.example.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.dal.domain.ChoiceQuestion;
import com.example.service.constants.Result;
import com.example.service.service.ChoiceQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * 选择题
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@RestController
@RequestMapping("/choice-question")
public class ChoiceQuestionController {
    @Autowired
    ChoiceQuestionService choiceQuestionService;
    /**
     * 查看所属章节的所有题目
     * http://localhost:9999/choice-question/user/selectChoiceQuestionByQuestionBankId/2
     */
    @GetMapping ("/user/selectChoiceQuestionByQuestionBankId/{id}")
    public Result userselectChoiceQuestionByQuestionBankId(@PathVariable String id){
        LambdaQueryWrapper<ChoiceQuestion> lambdaQueryWrapper=new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(ChoiceQuestion::getQuestionId,id);
        List<ChoiceQuestion> list = choiceQuestionService.list(lambdaQueryWrapper);
        if (list.size()>=3) {
            ArrayList<Integer> arrayList=new ArrayList<>();
            for (int i = 0; i <list.size() ; i++) {
                arrayList.add(i);
            }
            List<Integer> randoms = choiceQuestionService.generateUniqueLists(arrayList, 3);
            List<ChoiceQuestion> list3=new ArrayList<>();
            for (int i = 0; i <randoms.size() ; i++) {
                Integer integer = randoms.get(i);
                list3.add(list.get(integer)) ;
            }
            return Result.success(list3);
        }else
            return Result.fail(200,"该章节题目不足，请重新选择其他章节");
    }
    @GetMapping("/referee/selectChoiceQuestionByQuestionBankId/{id}")
    public Result refereeselectChoiceQuestionByQuestionBankId(@PathVariable String id){
        LambdaQueryWrapper<ChoiceQuestion> lambdaQueryWrapper=new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(ChoiceQuestion::getQuestionId,id);
        List<ChoiceQuestion> list = choiceQuestionService.list(lambdaQueryWrapper);
        if (list.size()>=3) {
            ArrayList<Integer> arrayList=new ArrayList<>();
            for (int i = 0; i <list.size() ; i++) {
                arrayList.add(i);
            }
            List<Integer> randoms = choiceQuestionService.generateUniqueLists(arrayList, 3);
            List<ChoiceQuestion> list3=new ArrayList<>();
            for (int i = 0; i <randoms.size() ; i++) {
                Integer integer = randoms.get(i);
                list3.add(list.get(integer)) ;
            }
            return Result.success(list3);
        }else
            return Result.fail(200,"该章节题目不足，请重新选择其他题库");
    }
    @GetMapping("/school/selectChoiceQuestionByQuestionBankId/{id}")
    public Result schoolselectChoiceQuestionByQuestionBankId(@PathVariable String id){
        LambdaQueryWrapper<ChoiceQuestion> lambdaQueryWrapper=new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(ChoiceQuestion::getQuestionId,id);
        List<ChoiceQuestion> list = choiceQuestionService.list(lambdaQueryWrapper);
        if (list.size()>=3) {
            ArrayList<Integer> arrayList=new ArrayList<>();
            for (int i = 0; i <list.size() ; i++) {
                arrayList.add(i);
            }
            List<Integer> randoms = choiceQuestionService.generateUniqueLists(arrayList, 3);
            List<ChoiceQuestion> list3=new ArrayList<>();
            for (int i = 0; i <randoms.size() ; i++) {
                Integer integer = randoms.get(i);
                list3.add(list.get(integer)) ;
            }
            return Result.success(list3);
        }else
            return Result.fail(200,"该章节题目不足，请重新选择其他题库");
    }




}
