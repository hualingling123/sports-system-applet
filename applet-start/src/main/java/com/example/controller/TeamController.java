package com.example.controller;


import com.example.dal.domain.AthleteApplication;
import com.example.dal.domain.AuditRecord;
import com.example.dal.domain.Team;
import com.example.service.constants.Result;
import com.example.service.domain.vo.UserVo;
import com.example.service.service.TeamService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hualingling
 * @since 2023-06-28
 */
@RestController
@RequestMapping("/team")
public class TeamController {
    @Autowired
    TeamService teamService;

    /*传入参数：
    {
        "groupName":"5"
    }
    */
    @PostMapping("/create")
    public Result createTeam(@RequestBody Team team){

        team.setIsDelete(false);
        team.setNumber(1);

        boolean result = teamService.createTeam(team);

        return Result.success("创建团队结果为:"+result);
    }

    /*  传入数据
    {
        "userVo":{
        "userId":2
    },
        "team":{
        "groupId":1
    }
    }*/
    @PutMapping("/join")
    public Result joinTeam(@RequestBody Map<String,Object> map){
        try{
            ObjectMapper objectMapper=new ObjectMapper();
            Team team = objectMapper.readValue(objectMapper.writeValueAsString(map.get("team")), Team.class);
            UserVo userVo = objectMapper.readValue(objectMapper.writeValueAsString(map.get("userVo")), UserVo.class);

            boolean result = teamService.joinTeam(team,userVo);

            return Result.success("加入团队结果为:"+result);
        }catch (Exception e){
            return Result.fail(500,"系统出现异常"+e);
        }
    }

    /*传入数据
    {
      "userId":2
    }
    */
    @PutMapping("/quit")
    public Result quitTeam(@RequestBody UserVo userVo){
        try{
            boolean result = teamService.quitTeam(userVo);
            return Result.success("退出团队结果为:"+result);
        }catch (Exception e){
            return Result.fail(500,"系统出现异常"+e);
        }
    }

    //直接调用
    @GetMapping()
    public List<Team> teamList(){
        return teamService.teamList();
    }

}
