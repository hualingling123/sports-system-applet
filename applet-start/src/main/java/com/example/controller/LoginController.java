package com.example.controller;

import com.example.service.constants.Result;
import com.example.service.domain.vo.LoginParam;
import com.example.service.service.LoginService;
import com.example.service.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: hualingling
 * @Date:2023/06/25/22:02
 */
@RestController
@RequestMapping("login")
public class LoginController {

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private LoginService loginService;

    @PostMapping
    public Result login(@RequestBody LoginParam loginParam){
        //登录 验证用户  访问用户表
        return loginService.login(loginParam);
    }

    @GetMapping("/user")
    public int user(){
        int a=2;

        return a;
    }
}