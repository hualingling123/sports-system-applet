package com.example.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.dal.domain.AthleteApplication;
import com.example.service.constants.Result;
import com.example.service.domain.vo.UserVo;
import com.example.service.service.AthleteApplicationService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import javax.security.auth.callback.LanguageCallback;
import java.util.List;
import java.util.Map;

/**
 * 用户申请
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@RestController
@RequestMapping("/athlete-application")
public class AthleteApplicationController {

    @Autowired
    AthleteApplicationService athleteApplicationService;

    /**
     * 传入参数
     * {
     *          "userVo":{
     *            "userName":"张三",
     *            "userId":"1"
     *         },
     *         "athleteApplication":{
     *             "userSchool":"广西职业技术学院",
     *              "applicationGoods":"这个是我的申请材料",
     *              "applicationReason":"这个是我的申请理由",
     *              "competitionId":"1"
     *          }
     *       }
     * @param map
     * @return
     * @throws JsonProcessingException
     */
    /**
     * 添加申请
     * @param map
     * @return
     * @throws JsonProcessingException
     */
    @PostMapping("/userAddAthleteApplication")
    public Result userAddAthleteApplication(@RequestBody Map<String,Object> map) throws JsonProcessingException {
       try{
           ObjectMapper objectMapper=new ObjectMapper();
           AthleteApplication athleteApplication = objectMapper.readValue(objectMapper.writeValueAsString(map.get("athleteApplication")), AthleteApplication.class);
           UserVo userVo = objectMapper.readValue(objectMapper.writeValueAsString(map.get("userVo")), UserVo.class);
           athleteApplication.setUserId(userVo.getUserId());
           athleteApplication.setName(userVo.getUserName());
           boolean save = athleteApplicationService.save(athleteApplication);
           return Result.success("添加"+save);
       }catch (Exception e){
           return Result.fail(500,"系统出现异常");
       }
    }
    //查看自己的申请

    /**
     * 传入参数
     * {
     *     "userId":1
     * }
     * @param userVo
     * @return
     * @throws JsonProcessingException
     */
    @GetMapping("/userSelectMyAthleteApplication")
    public Result userSelectMyAthleteApplication(@RequestBody UserVo userVo) throws JsonProcessingException {
        LambdaQueryWrapper<AthleteApplication> lambdaQueryWrapper=new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(AthleteApplication::getUserId,userVo.getUserId());
        List<AthleteApplication> list = athleteApplicationService.list(lambdaQueryWrapper);
        return Result.success(list);
    }

    //修改自己的申请

    /**
     *传入参数
     * {
     *          "userVo":{
     *            "userName":"张三",
     *            "userId":"1"
     *         },
     *         "athleteApplication":{
     *             "userSchool":"广西职业技术学院",
     *              "applicationGoods":"这个是我的申请材料1111",
     *              "applicationReason":"这个是我的申请理由1111",
     *              "competitionId":"1"
     *          }
     *       }
     * @param map
     * @return
     * @throws JsonProcessingException
     */
    @PutMapping("/updateAthleteApplication")
    public Result updateAthleteApplication(@RequestBody Map<String,Object> map ) throws JsonProcessingException {
     try{
         ObjectMapper objectMapper=new ObjectMapper();
         AthleteApplication athleteApplication = objectMapper.readValue(objectMapper.writeValueAsString(map.get("athleteApplication")), AthleteApplication.class);
         UserVo userVo = objectMapper.readValue(objectMapper.writeValueAsString(map.get("userVo")), UserVo.class);
         LambdaQueryWrapper<AthleteApplication> lambdaQueryWrapper=new LambdaQueryWrapper<>();
         lambdaQueryWrapper.eq(AthleteApplication::getUserId,userVo.getUserId()).eq(AthleteApplication::getId,athleteApplication.getId());
         athleteApplication.setUserId(userVo.getUserId());
         athleteApplication.setName(userVo.getUserName());
         boolean update = athleteApplicationService.update(athleteApplication, lambdaQueryWrapper);
         if (update){
             return Result.success("修改"+update);
         }else {
             return Result.fail(500,"你没有这个申请");
         }
     }catch (Exception e){
         return Result.fail(500,"系统出现异常");
     }
     }




}
