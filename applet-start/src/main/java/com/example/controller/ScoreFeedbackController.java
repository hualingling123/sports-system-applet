package com.example.controller;


import com.example.dal.domain.ScoreFeedback;
import com.example.service.constants.Result;
import com.example.service.domain.vo.UserVo;
import com.example.service.service.ScoreFeedbackService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.Map;

/**
 * 成绩问题反馈表
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@RestController
@RequestMapping("/score-feedback")
public class ScoreFeedbackController {

    @Autowired
    ScoreFeedbackService scoreFeedbackService;
    //运动员成绩反馈

    /**
     * 传入参数
     * {
     *   "scoreFeedback": {
     *       "feedbackContent":"成绩有问题"
     *   },
     *   "userVo": {
     *     "userName":"张三",
     *     "userId":"2"
     *   }
     * }
     * @param map
     * @return
     * @throws JsonProcessingException
     */
    @PostMapping("/user/userAddScoreFeedbackController")
    public Result userAddScoreFeedbackController(@RequestBody Map<String,Object> map) throws JsonProcessingException {
     try {
//        用map获取的是Object对象，这里要转指定对象要通过ObjectMapper来作为介质来转
        ObjectMapper objectMapper = new ObjectMapper();
        ScoreFeedback scoreFeedback = objectMapper.readValue(objectMapper.writeValueAsString(map.get("scoreFeedback")), ScoreFeedback.class);
        UserVo userVo = objectMapper.readValue(objectMapper.writeValueAsString(map.get("userVo")), UserVo.class);
         String userid = userVo.getUserId();
         scoreFeedback.setStudentId(userid);
         scoreFeedback.setStudentName(userVo.getUserName());
         scoreFeedbackService.save(scoreFeedback);
         return  Result.success("ok");
     }catch (Exception e){
         return Result.fail(500,"系统出现异常");
     }
    }




}
