package com.example.controller;


import com.example.dal.domain.CompetitionInformation;
import com.example.dal.domain.CompetitionScores;
import com.example.service.constants.Result;
import com.example.service.domain.vo.UserVo;
import com.example.service.service.CompetitionScoresService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import javax.jws.soap.SOAPBinding;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hualingling
 * @since 2023-07-07
 */
@RestController
@RequestMapping("/competition-scores")
public class CompetitionScoresController {
    @Autowired
    CompetitionScoresService competitionScoresService;

    /*  测试传参,如果用户不是运动员则会返回null值
    {
        "userId":1
    }
    */
// 获取个人成绩
    @GetMapping("/")
    public List<CompetitionScores> getScore(@RequestBody UserVo userVo){
        try{
            return competitionScoresService.getScore(userVo);
        }catch (Exception e){
            System.out.println(e);
            return null;
        }

    }

}
