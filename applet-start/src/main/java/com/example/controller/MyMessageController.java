package com.example.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.dal.domain.UserInfo;
import com.example.service.constants.Result;
import com.example.service.domain.vo.UserVo;
import com.example.service.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.security.auth.callback.LanguageCallback;

@RestController
public class MyMessageController {
    @Autowired
    UserInfoService userInfoService;

    /**
     * 展示我的个人信息
     * @param userVo
     * @return
     */
    @RequestMapping("User/selectByidMyMessage")
    public Result UserSelectByidMyMessage(@RequestBody UserVo userVo){
        try{
            UserInfo userByUserid = userInfoService.findUserByUserId(userVo.getUserId());
            return Result.success(userByUserid);
        }catch (Exception e){
            return Result.fail(500,"系统出现异常");
        }
    }
    @RequestMapping("Referee/SelectByidMyMessage")
    public Result RefereeSelectByidMyMessage(@RequestBody UserVo userVo){
        try {
            LambdaQueryWrapper<UserInfo> lambdaQueryWrapper=new LambdaQueryWrapper<>();
            lambdaQueryWrapper.eq(UserInfo::getUserId,userVo.getUserId());
            UserInfo userByUserId = userInfoService.findUserByUserId(userVo.getUserId());
            return Result.success(userByUserId);
        }catch (Exception e){
            return Result.fail(500,"系统出现异常");
        }
    }
    @RequestMapping("School/SelectByidMyMessage")
    public Result SchoolSelectByidMyMessage(@RequestBody UserVo userVo){
        try{
            LambdaQueryWrapper<UserInfo> lambdaQueryWrapper=new LambdaQueryWrapper<>();
            lambdaQueryWrapper.eq(UserInfo::getUserId,userVo.getUserId());
            UserInfo userByUserid = userInfoService.findUserByUserId(userVo.getUserId());
            return Result.success(userByUserid);
        }catch (Exception e){
            return Result.fail(500,"系统出现异常");
        }
    }

}
