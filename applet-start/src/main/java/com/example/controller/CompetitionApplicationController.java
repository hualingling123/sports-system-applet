package com.example.controller;


import com.example.dal.domain.AthleteApplication;
import com.example.dal.domain.AuditRecord;
import com.example.dal.domain.CompetitionInformation;
import com.example.service.constants.Result;
import com.example.service.domain.vo.UserVo;
import com.example.service.service.CompetitionApplicationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hualingling
 * @since 2023-07-03
 */
@RestController
@RequestMapping("/competition-application")
public class CompetitionApplicationController {
    @Autowired
    CompetitionApplicationService competitionApplicationService;

    /* 传入参数
    {
        "userVo":{
        "userId":3,
                "userName":"x"
    },
        "competitionInformation":{
        "competitionId":1
    }
    }
    */
    @PutMapping("/joinCompetition")
    public Result applicationCompetition(@RequestBody Map<String,Object> map){
        try{
            ObjectMapper objectMapper=new ObjectMapper();
            CompetitionInformation competitionInformation = objectMapper.readValue(objectMapper.writeValueAsString(map.get("competitionInformation")), CompetitionInformation.class);
            UserVo userVo = objectMapper.readValue(objectMapper.writeValueAsString(map.get("userVo")), UserVo.class);

            boolean result = competitionApplicationService.joinCompetition(competitionInformation,userVo);
            return Result.success("申请比赛状态:"+result);
        }catch (Exception e){
            return Result.fail(500,"系统出现异常"+e);
        }
    }
}
