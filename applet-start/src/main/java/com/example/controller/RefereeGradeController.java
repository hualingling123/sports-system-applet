package com.example.controller;


import com.example.dal.domain.RefereeGrade;
import com.example.service.constants.Result;
import com.example.service.domain.vo.UserVo;
import com.example.service.service.RefereeGradeService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 裁判打分
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@RestController
@RequestMapping("/referee-grade")
public class RefereeGradeController {
    @Autowired
    RefereeGradeService refereeGradeService;
    //传入
    /**
     * 测试传参
     * {
     *     "userVo":{
     *         "userName":"张三",
     *         "userId":"1"
     *     },
     *     "refereeGrade":{
     *         "score":"100",
     *         "competitionId":"1",
     *         "competitionName":"长跑"
     *     }
     * }
     * @param map
     * @return
     * @throws JsonProcessingException
     */
    @PostMapping("/addRefereeGrade")
    public Result addRefereeGrade(@RequestBody Map<String,Object>map) throws JsonProcessingException {
        try{
            ObjectMapper objectMapper=new ObjectMapper();
            UserVo userVo = objectMapper.readValue(objectMapper.writeValueAsString(map.get("userVo")), UserVo.class);
            RefereeGrade refereeGrade = objectMapper.readValue(objectMapper.writeValueAsString(map.get("refereeGrade")), RefereeGrade.class);
            refereeGrade.setRefereeId(userVo.getUserId());
            refereeGrade.setRefereeName(userVo.getUserName());
            boolean save = refereeGradeService.save(refereeGrade);
            return Result.success(save+"成功");
        }catch (Exception e){
            return Result.fail(500,"系统添加异常");
        }
    }
}
