package com.example.controller;


import com.example.dal.domain.AdminFeedback;
import com.example.dal.domain.AthleteApplication;
import com.example.service.constants.Result;
import com.example.service.domain.vo.UserVo;
import com.example.service.service.AdminFeedbackService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 * 系统问题反馈
 *
 * @author hualingling
 * @since 2023-06-25
 */
@RestController
@RequestMapping("/admin-feedback")
public class AdminFeedbackController {

    @Autowired
    AdminFeedbackService adminFeedbackService;

    /**
     * {
     *     "userVo":{
     *         "userId":"1",
     *         "userName":"张三",
     *         "school":"广西职业技术学院"
     *     },
     *    "adminFeedback": {
     *        "feedbackpersonName":"反馈标题1",
     *        "feedbackContent":"反馈内容"
     *
     *     }
     * }
     * @param
     * @return
     */
    @PostMapping("/user/AdminFeedback")
    public Result UserAddFeedback(@RequestBody Map<String,Object> map){
        try{
            ObjectMapper objectMapper=new ObjectMapper();
            AdminFeedback adminFeedback = objectMapper.readValue(objectMapper.writeValueAsString(map.get("adminFeedback")), AdminFeedback.class);
            UserVo userVo = objectMapper.readValue(objectMapper.writeValueAsString(map.get("userVo")), UserVo.class);
            adminFeedback.setFeedbackName(userVo.getUserName());
            adminFeedback.setSchool(userVo.getSchool());
            adminFeedbackService.save(adminFeedback);
            return Result.success("ok");
        }catch (Exception e){
            return Result.fail(200,"系统出现异常");
        }
    }
    @PostMapping("/referee/AdminFeedback")
    public Result refereeAddFeedback(@RequestBody Map<String,Object> map){
        try{
            ObjectMapper objectMapper=new ObjectMapper();
            AdminFeedback adminFeedback = objectMapper.readValue(objectMapper.writeValueAsString(map.get("adminFeedback")), AdminFeedback.class);
            UserVo userVo = objectMapper.readValue(objectMapper.writeValueAsString(map.get("userVo")), UserVo.class);
            adminFeedback.setFeedbackName(userVo.getUserName());
            adminFeedback.setSchool(userVo.getSchool());
            adminFeedbackService.save(adminFeedback);
            return Result.success("ok");
        }catch (Exception e){
            return Result.fail(200,"系统出现异常");
        }
    }
    @PostMapping("/school/AdminFeedback")
    public Result schoolAddFeedback(@RequestBody Map<String,Object> map){
        try{
            ObjectMapper objectMapper=new ObjectMapper();
            AdminFeedback adminFeedback = objectMapper.readValue(objectMapper.writeValueAsString(map.get("adminFeedback")), AdminFeedback.class);
            UserVo userVo = objectMapper.readValue(objectMapper.writeValueAsString(map.get("userVo")), UserVo.class);
            adminFeedback.setFeedbackName(userVo.getUserName());
            adminFeedback.setSchool(userVo.getSchool());
            adminFeedbackService.save(adminFeedback);
            return Result.success("ok");
        }catch (Exception e){
            return Result.fail(200,"系统出现异常");
        }
    }

}
