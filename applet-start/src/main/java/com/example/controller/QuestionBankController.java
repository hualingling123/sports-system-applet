package com.example.controller;


import com.example.dal.domain.QuestionBank;
import com.example.service.constants.Result;
import com.example.service.service.QuestionBankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hualingling
 * @since 2023-06-25
 */
@RestController
@RequestMapping("/question-bank")
public class QuestionBankController {
    @Autowired
    QuestionBankService questionBankService;
    //查找所有章节
    @Autowired
    @RequestMapping("/user/selectAllQuestionBank")
    public Result userSelectAllQuestionBank(){
        try{
            List<QuestionBank> list = questionBankService.list(null);
            return Result.success(list);
        }catch (Exception e){
            return Result.fail(200,"系统异常");
        }
    }

    @RequestMapping("/referee/selectAllQuestionBank")
    public Result refereeSelectAllQuestionBank(){
        try{
            List<QuestionBank> list = questionBankService.list(null);
            return Result.success(list);
        }catch (Exception e){
            return Result.fail(200,"系统异常");
        }
    }
    @RequestMapping("/school/SelectAllQuestionBank")
    public Result schoolSelectAllQuestionBank(){
        try{
            List<QuestionBank> list = questionBankService.list(null);
            return Result.success(list);
        }catch (Exception e){
            return Result.fail(200,"系统异常");
        }
    }

}
