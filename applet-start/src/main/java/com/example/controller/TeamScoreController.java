package com.example.controller;

import com.example.dal.domain.CompetitionInformation;
import com.example.dal.domain.TeamScore;
import com.example.service.service.TeamScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/teamRank")
public class TeamScoreController {
    @Autowired
    TeamScoreService teamScoreService;

    /*   传入数据
    {
        "competitionId":1
    }
    */
    @GetMapping()
    public List<TeamScore> teamRank(@RequestBody CompetitionInformation competitionInformation){
        return teamScoreService.teamRank(competitionInformation.getCompetitionId());
    }

}
