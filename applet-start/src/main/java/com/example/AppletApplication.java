package com.example;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: hualingling
 * @Date:2023/06/24/19:39
 */

@SpringBootApplication
@MapperScan("com.example.dal.mapper")
public class AppletApplication {
    public static void main(String[] args) {
        SpringApplication.run(AppletApplication.class, args);
    }

}

